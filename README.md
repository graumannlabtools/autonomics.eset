[![Project Status: Wip - Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](http://www.repostatus.org/badges/0.1.0/wip.svg)](http://www.repostatus.org/#wip)

# autonomics.eset

S4 classes and methods for esets.    
It is part of the [*autonomics*](hhttps://bitbucket.org/graumannlabtools/autonomics) suite of packages.

## Installation

To install the package, you first need the
[*devtools*](https://github.com/hadley/devtools) package.

```{r}
install.packages("devtools")
```
Then you can install the *autonomics.eset* package using

```{r}
library(devtools)
install_bitbucket("graumannlabtools/autonomics.eset")
```


## Functions

- `fdata`  get/set eset feature data
- `sdata`  get/set eset sample data
- `fnames` get/set eset feature names
- `snames` get/set eset sample names
- `fvars`  get/set eset feature variables
- `svars`  get/set eset sample variables
- `exprs`  get/set eset expr matrix