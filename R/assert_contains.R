# assert that eSet is valid and 'comics-ready'
#---------------------------------------------

is_summarized_experiment <- function(x, .xname = assertive.base::get_name_in_parent(x)){
   .Deprecated('autonomics.import::is_summarized_experiment')
   if (!methods::is(x, 'SummarizedExperiment')){
      return(assertive.base::false('%s is not a SummarizedExperiment', .xname))
   }
   TRUE
}

is_eSet <- function(x, .xname = assertive.base::get_name_in_parent(x)){
   .Deprecated('autonomics.import::is_eSet')
   if (!methods::is(x, 'eSet')){
      return(assertive.base::false('%s is not an eSet', .xname))
   }
   TRUE
}

is_elist <- function(x, .xname = assertive.base::get_name_in_parent(x)){
   .Deprecated('autonomics.import::is_elist')
   if (!methods::is(x, 'EList')){
    return(assertive.base::false('%s is not an EList', .xname))
  }
  TRUE
}

has_valid_featureNames <- function(x, .xname = assertive.base::get_name_in_parent(x)){
   .Deprecated('autonomics.import::has_valid_featureNames')
   # SummarizedExperiments do not allow row naming of fdata
   # if (!all(autonomics.eset::fnames(x) == rownames(autonomics.eset::fdata(x)))){
   #   return(assertive.base::false('fnames(%s) differ from rownames(fdata(%s))', .xname, .xname))
   # }
   if (!all(autonomics.eset::fnames(x) == rownames(autonomics.eset::exprs(x)))){
      return(assertive.base::false('fnames(%s) differ from rownames(exprs(%s))', .xname, .xname))
   }
   TRUE
}

has_valid_sampleNames <- function(x, .xname = assertive.base::get_name_in_parent(x)){
   .Deprecated('autonomics.import::has_valid_sampleNames')
   if (!all(autonomics.eset::snames(x) == rownames(autonomics.eset::sdata(x)))){
      return(assertive.base::false('snames(%s) differ from rownames(sdata(%s))', .xname, .xname))
   }
   if (!all(autonomics.eset::snames(x) == colnames(autonomics.eset::exprs(x)))){
      return(assertive.base::false('snames(%s) differ from colnames(exprs(%s))', .xname, .xname))
   }
   TRUE
}


#' Is valid eset?
#' @param x eset
#' @param .xname see assertive.base::get_name_in_parent
#' @return logical
#' @export
is_valid_eset <- function(x, .xname = assertive.base::get_name_in_parent(x)){
   .Deprecated('autonomics.import::is_valid_eset')   
   if (!(ok <- is_eSet(x, .xname = .xname)  |  is_elist(x, .xname = .xname)  |  is_summarized_experiment(x, .xname = .xname))){   return(ok)}
   if (!(ok <- has_valid_featureNames(x, .xname = .xname)))                  {   return(ok)}
   if (!(ok <- has_valid_sampleNames(x,  .xname = .xname)))                  {   return(ok)}
   TRUE
}

#' Assert that x is a valid eSet
#' @param x eset
#' @return error if not true
#' @export
assert_is_valid_eset <- function(x){
   .Deprecated('autonomics.import::assert_is_valid_eset')   
   assertive.base::assert_engine(is_valid_eset, x, .xname = assertive.base::get_name_in_parent(x))
}

#' Assert that features are valid
#' @param features \code{\link{numeric}} index or \code{\link{character}} names
#' of \code{\link[autonomics.eset]{fdata}} entries
#' @param object eSet
#' @importFrom magrittr %>%
#' @export
assert_all_are_valid_features <- function(features, object){
   .Deprecated('autonomics.import::assert_all_are_valid_features')
   if (is.character(features)) {
      features %>%
      assertive.sets::assert_is_subset(
         object %>%
         autonomics.eset::fdata() %>%
         magrittr::extract2("feature_id"))
   } else if (is.numeric(features)) {
      features %>%
      assertive.numbers::assert_all_are_whole_numbers() %>%
      assertive.numbers::assert_all_are_in_closed_range(1, nrow(object))
   } else {
      stop("Features must be numeric indexes or character objects.")
   }
   features %>%
   invisible()
}


#' Does eset contain preprocessing info?
#' @param object eset
#' @return logical
#' @export
#' @examples
#' require(magrittr)
#' if (require(billing.differentiation.data)){
#'    billing.differentiation.data::rna.voomcounts %>% contains_prepro()
#' }
contains_prepro <- function(object){
   .Deprecated('autonomics.import::contains_prepro')
   autonomics.eset::assert_is_valid_eset(object)
   length(autonomics.eset::prepro(object))!=0
}


#' Does object contain block values?
#' @param object SummarizedExperiment
#' @return logical(1)
#' @export
contains_block <- function(object){
   .Deprecated('autonomics.import::contains_block')
   block_in_svars <- 'block' %in% autonomics.eset::svars(object)
   if (!block_in_svars) return(FALSE)
   block_levels <- object %>% autonomics.eset::svalues('block')
   if ("" %in% block_levels) return(FALSE)
   return(TRUE)
}